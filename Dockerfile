FROM php:cli-alpine

RUN apk add curl-dev oniguruma-dev
RUN docker-php-ext-install curl mbstring
RUN apk add --no-cache --virtual .phpize-deps-configure $PHPIZE_DEPS
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN apk del --no-network .phpize-deps-configure

RUN mkdir /app
VOLUME /app
WORKDIR /app
