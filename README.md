# Athenapdf-client

PHP client library for [Athenapdf service](https://github.com/arachnys/athenapdf).

## Installation with composer:

```shell
composer install crocodile2u/athenapdf-client
```

## Run tests & development setup

```shell
cd /path/to/athenapdf-client
```

1. Make sure have a compatible version of PHP installed locally: 7.4, 8.0 or 8.1.
2. Run `composer install` locally.

To run tests:

```shell
docker-compose up -d
docker-compose exec test ./vendor/bin/phpunit
```

## Known problems

`arachnysdocker/athenapdf-service` docker image might not run on a M1 processor.
If I get access to a Mac, I might want to publish an image for M1 users one day.
