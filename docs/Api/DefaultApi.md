# Athenapdf\DefaultApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**convertByFileUpload()**](DefaultApi.md#convertByFileUpload) | **POST** /convert |  |
| [**convertByUrl()**](DefaultApi.md#convertByUrl) | **GET** /convert |  |
| [**stats()**](DefaultApi.md#stats) | **GET** /stats |  |
| [**status()**](DefaultApi.md#status) | **GET** / |  |


## `convertByFileUpload()`

```php
convertByFileUpload($file, $ext): \SplFileObject
```



upload file and convert contents to PDF

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = Athenapdf\Configuration::getDefaultConfiguration()->setApiKey('auth', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Athenapdf\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth', 'Bearer');


$apiInstance = new Athenapdf\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$file = "/path/to/file.txt"; // \SplFileObject
$ext = 'ext_example'; // string

try {
    $result = $apiInstance->convertByFileUpload($file, $ext);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->convertByFileUpload: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **file** | **\SplFileObject****\SplFileObject**|  | |
| **ext** | **string**|  | [optional] |

### Return type

**\SplFileObject**

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/pdf`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `convertByUrl()`

```php
convertByUrl($url): \SplFileObject
```



fetch URL and convert contents to PDF

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = Athenapdf\Configuration::getDefaultConfiguration()->setApiKey('auth', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Athenapdf\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth', 'Bearer');


$apiInstance = new Athenapdf\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$url = 'url_example'; // string

try {
    $result = $apiInstance->convertByUrl($url);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->convertByUrl: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **url** | **string**|  | |

### Return type

**\SplFileObject**

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/pdf`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stats()`

```php
stats(): \Athenapdf\Model\Stats200Response
```



statistics

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = Athenapdf\Configuration::getDefaultConfiguration()->setApiKey('auth', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Athenapdf\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth', 'Bearer');


$apiInstance = new Athenapdf\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->stats();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->stats: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Athenapdf\Model\Stats200Response**](../Model/Stats200Response.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `status()`

```php
status(): \Athenapdf\Model\Status200Response
```



status

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: ApiKeyAuth
$config = Athenapdf\Configuration::getDefaultConfiguration()->setApiKey('auth', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Athenapdf\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth', 'Bearer');


$apiInstance = new Athenapdf\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->status();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->status: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Athenapdf\Model\Status200Response**](../Model/Status200Response.md)

### Authorization

[ApiKeyAuth](../../README.md#ApiKeyAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
