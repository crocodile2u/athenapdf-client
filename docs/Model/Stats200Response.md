# # Stats200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**goroutines** | **int** |  | [optional]
**pending** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
